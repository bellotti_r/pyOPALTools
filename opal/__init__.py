from .opal import load_dataset
from .opal import filetype
import opal.analysis
import opal.datasets
import opal.parser
import opal.utilities
import opal.visualization
import opal.config
