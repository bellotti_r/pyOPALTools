opal package
============

Subpackages
-----------

.. toctree::

    opal.analysis
    opal.datasets
    opal.parser
    opal.utilities
    opal.visualization

Submodules
----------

opal.config module
------------------

.. automodule:: opal.config
    :members:
    :undoc-members:
    :show-inheritance:

opal.opal module
----------------

.. automodule:: opal.opal
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: opal
    :members:
    :undoc-members:
    :show-inheritance:
