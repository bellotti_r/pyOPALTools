amr package
===========

Submodules
----------

amr.AmrOpal module
------------------

.. automodule:: amr.AmrOpal
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: amr
    :members:
    :undoc-members:
    :show-inheritance:
