opal.visualization package
==========================

Subpackages
-----------

.. toctree::

    opal.visualization.statistics
    opal.visualization.styles

Submodules
----------

opal.visualization.AmrPlotter module
------------------------------------

.. automodule:: opal.visualization.AmrPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.BasePlotter module
-------------------------------------

.. automodule:: opal.visualization.BasePlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.GridPlotter module
-------------------------------------

.. automodule:: opal.visualization.GridPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.H5Plotter module
-----------------------------------

.. automodule:: opal.visualization.H5Plotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.MemoryPlotter module
---------------------------------------

.. automodule:: opal.visualization.MemoryPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.OptimizerPlotter module
------------------------------------------

.. automodule:: opal.visualization.OptimizerPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.PeakPlotter module
-------------------------------------

.. automodule:: opal.visualization.PeakPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.ProbePlotter module
--------------------------------------

.. automodule:: opal.visualization.ProbePlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.ProfilingPlotter module
------------------------------------------

.. automodule:: opal.visualization.ProfilingPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.SamplerPlotter module
----------------------------------------

.. automodule:: opal.visualization.SamplerPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.SolverPlotter module
---------------------------------------

.. automodule:: opal.visualization.SolverPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.StatPlotter module
-------------------------------------

.. automodule:: opal.visualization.StatPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.StdOpalOutputPlotter module
----------------------------------------------

.. automodule:: opal.visualization.StdOpalOutputPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.TimingPlotter module
---------------------------------------

.. automodule:: opal.visualization.TimingPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.TrackOrbitPlotter module
-------------------------------------------

.. automodule:: opal.visualization.TrackOrbitPlotter
    :members:
    :undoc-members:
    :show-inheritance:

opal.visualization.formatter module
-----------------------------------

.. automodule:: opal.visualization.formatter
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: opal.visualization
    :members:
    :undoc-members:
    :show-inheritance:
