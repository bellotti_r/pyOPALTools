opal.analysis package
=====================

Submodules
----------

opal.analysis.H5Statistics module
---------------------------------

.. automodule:: opal.analysis.H5Statistics
    :members:
    :undoc-members:
    :show-inheritance:

opal.analysis.SamplerStatistics module
--------------------------------------

.. automodule:: opal.analysis.SamplerStatistics
    :members:
    :undoc-members:
    :show-inheritance:

opal.analysis.Statistics module
-------------------------------

.. automodule:: opal.analysis.Statistics
    :members:
    :undoc-members:
    :show-inheritance:

opal.analysis.StdOpalOutputAnalysis module
------------------------------------------

.. automodule:: opal.analysis.StdOpalOutputAnalysis
    :members:
    :undoc-members:
    :show-inheritance:

opal.analysis.TrackOrbitAnalysis module
---------------------------------------

.. automodule:: opal.analysis.TrackOrbitAnalysis
    :members:
    :undoc-members:
    :show-inheritance:

opal.analysis.cyclotron module
------------------------------

.. automodule:: opal.analysis.cyclotron
    :members:
    :undoc-members:
    :show-inheritance:

opal.analysis.impl\_beam module
-------------------------------

.. automodule:: opal.analysis.impl_beam
    :members:
    :undoc-members:
    :show-inheritance:

opal.analysis.pareto\_fronts module
-----------------------------------

.. automodule:: opal.analysis.pareto_fronts
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: opal.analysis
    :members:
    :undoc-members:
    :show-inheritance:
