opal.visualization.statistics package
=====================================

Submodules
----------

opal.visualization.statistics.impl\_plots module
------------------------------------------------

.. automodule:: opal.visualization.statistics.impl_plots
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: opal.visualization.statistics
    :members:
    :undoc-members:
    :show-inheritance:
