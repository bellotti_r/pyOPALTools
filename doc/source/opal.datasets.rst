opal.datasets package
=====================

Submodules
----------

opal.datasets.AmrDataset module
-------------------------------

.. automodule:: opal.datasets.AmrDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.DatasetBase module
--------------------------------

.. automodule:: opal.datasets.DatasetBase
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.GridDataset module
--------------------------------

.. automodule:: opal.datasets.GridDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.H5Dataset module
------------------------------

.. automodule:: opal.datasets.H5Dataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.LBalDataset module
--------------------------------

.. automodule:: opal.datasets.LBalDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.LossDataset module
--------------------------------

.. automodule:: opal.datasets.LossDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.MemoryDataset module
----------------------------------

.. automodule:: opal.datasets.MemoryDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.OptimizerDataset module
-------------------------------------

.. automodule:: opal.datasets.OptimizerDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.PeakDataset module
--------------------------------

.. automodule:: opal.datasets.PeakDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.ProbeHistDataset module
-------------------------------------

.. automodule:: opal.datasets.ProbeHistDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.SDDSDatasetBase module
------------------------------------

.. automodule:: opal.datasets.SDDSDatasetBase
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.SamplerDataset module
-----------------------------------

.. automodule:: opal.datasets.SamplerDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.SolverDataset module
----------------------------------

.. automodule:: opal.datasets.SolverDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.StatDataset module
--------------------------------

.. automodule:: opal.datasets.StatDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.StdOpalOutputDataset module
-----------------------------------------

.. automodule:: opal.datasets.StdOpalOutputDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.TimeDataset module
--------------------------------

.. automodule:: opal.datasets.TimeDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.TrackOrbitDataset module
--------------------------------------

.. automodule:: opal.datasets.TrackOrbitDataset
    :members:
    :undoc-members:
    :show-inheritance:

opal.datasets.filetype module
-----------------------------

.. automodule:: opal.datasets.filetype
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: opal.datasets
    :members:
    :undoc-members:
    :show-inheritance:
